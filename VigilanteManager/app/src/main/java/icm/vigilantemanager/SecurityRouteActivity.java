package icm.vigilantemanager;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SecurityRouteActivity extends AppCompatActivity {

    private DatabaseReference mDatabase;
    public String id;
    public Button button;
    public EditText address;
    public Button button2;
    public EditText address2;
    public TextView destCoord;
    public TextView origCoord;

    public Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_security_route);

        mDatabase = FirebaseDatabase.getInstance().getReference();

        Segurancas user = (Segurancas) getIntent().getExtras().get("item");
        origCoord = (TextView) findViewById(R.id.origCoord);
        destCoord = (TextView) findViewById(R.id.destCoord);

        id = user.getUserID();

        button = (Button)findViewById(R.id.originButton);
        address = (EditText)findViewById(R.id.originAddress);
        button2 = (Button)findViewById(R.id.destButton);
        address2 = (EditText)findViewById(R.id.destAddress);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new GetOriginCoordinates().execute(address.getText().toString().replace(" ","+"));
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new GetDestinyCoordinates().execute(address2.getText().toString().replace(" ","+"));
            }
        });


    }

    public void getUser()
    {

        mDatabase.child("coordenadas").child(id).child("OrigemLat").setValue(0.0);
        mDatabase.child("coordenadas").child(id).child("OrigemLon").setValue(0.0);
        mDatabase.child("coordenadas").child(id).child("DestinoLat").setValue(0.0);
        mDatabase.child("coordenadas").child(id).child("DestinoLon").setValue(0.0);
    }


    private class GetOriginCoordinates extends AsyncTask<String,Void,String> {

        @Override
        protected String doInBackground(String... strings) {
            String response;
            try{
                String address = strings[0];
                HttpDataHandler http = new HttpDataHandler();
                String url = String.format("https://maps.googleapis.com/maps/api/geocode/json?address=%s",address);
                response = http.getHTTPData(url);
                return response;
            }
            catch (Exception ex)
            { }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            try{
                JSONObject jsonObject = new JSONObject(s);

                String lat = ((JSONArray)jsonObject.get("results")).getJSONObject(0).getJSONObject("geometry")
                        .getJSONObject("location").get("lat").toString();
                String lng = ((JSONArray)jsonObject.get("results")).getJSONObject(0).getJSONObject("geometry")
                        .getJSONObject("location").get("lng").toString();

                saveOrigin(lat, lng);
                origCoord.setText(String.format("Coordinates : %s / %s ",lat,lng));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private class GetDestinyCoordinates extends AsyncTask<String,Void,String> {

        @Override
        protected String doInBackground(String... strings) {
            String response;
            try{
                String address = strings[0];
                HttpDataHandler http = new HttpDataHandler();
                String url = String.format("https://maps.googleapis.com/maps/api/geocode/json?address=%s",address);
                response = http.getHTTPData(url);
                return response;
            }
            catch (Exception ex)
            { }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            try{
                JSONObject jsonObject = new JSONObject(s);

                String lat = ((JSONArray)jsonObject.get("results")).getJSONObject(0).getJSONObject("geometry")
                        .getJSONObject("location").get("lat").toString();
                String lng = ((JSONArray)jsonObject.get("results")).getJSONObject(0).getJSONObject("geometry")
                        .getJSONObject("location").get("lng").toString();

                destCoord.setText(String.format("Coordinates : %s / %s ",lat,lng));
                saveDestiny(lat,lng);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void saveOrigin(String lat, String lng)
    {
        Double lati = Double.parseDouble(lat);
        Double lon = Double.parseDouble(lng);

        mDatabase.child("coordenadas").child(id).child("OrigemLat").setValue(lati);
        mDatabase.child("coordenadas").child(id).child("OrigemLon").setValue(lon);
    }

    public void saveDestiny(String lat, String lng)
    {
        Double lati = Double.parseDouble(lat);
        Double lon = Double.parseDouble(lng);

        mDatabase.child("coordenadas").child(id).child("DestinoLat").setValue(lati);
        mDatabase.child("coordenadas").child(id).child("DestinoLon").setValue(lon);
    }
}
