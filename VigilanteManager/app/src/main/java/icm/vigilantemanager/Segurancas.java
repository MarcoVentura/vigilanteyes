package icm.vigilantemanager;

import java.io.Serializable;

/**
 * Created by Marco on 11/11/2017.
 */

public class Segurancas implements Serializable{

    private String nome;
    private Double lat;
    private Double lon;
    private Double origem;
    private Double destino;
    private String key;
    private String userID;

    public Segurancas() {}

    public Segurancas(String nome, Double lat, Double lon) {
        this.nome = nome;
        this.lat = lat;
        this.lon = lon;
    }


    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public String getUserID() {
        return userID;
    }
    public void setUserID(String id) {
        this.userID= id;
    }

    @Override
    public String toString() {
        return nome;
    }
}
