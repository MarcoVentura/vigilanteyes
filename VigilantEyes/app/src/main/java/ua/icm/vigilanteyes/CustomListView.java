package ua.icm.vigilanteyes;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Marco on 09/10/2017.
 */

public class CustomListView extends ArrayAdapter<String> {

    private String [] atividades;
    private Integer [] imagens;
    private Activity context;

    public CustomListView(@NonNull Activity context, String [] atividades, Integer [] imagens) {
        super(context, R.layout.list_item, atividades);

        this.context = context;
        this.atividades = atividades;
        this.imagens = imagens;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View r = convertView;
        ViewHolder viewHolder = null;

        if (r == null) {
            LayoutInflater layoutInflater = context.getLayoutInflater();
            r = layoutInflater.inflate(R.layout.list_item, null, true);
            viewHolder = new ViewHolder(r);
            r.setTag(viewHolder);
        }
        else
            viewHolder = (ViewHolder) r.getTag();

        viewHolder.imageView.setImageResource(imagens[position]);
        viewHolder.textView.setText(atividades[position]);
        return r;
    }

    class ViewHolder {
        TextView textView;
        ImageView imageView;

        ViewHolder(View v) {
            textView = (TextView) v.findViewById(R.id.textoID);
            imageView = (ImageView) v.findViewById(R.id.imagemID);
        }
    }
}
