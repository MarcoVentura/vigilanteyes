package ua.icm.vigilanteyes;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;

/**
 * Created by Marco on 01/11/2017.
 */

@SuppressWarnings("ALL")
public class MonitoringFragment extends Fragment {

    // Firebase instance variables
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;

    private String mUsername;
    private String mPhotoUrl;
    private SharedPreferences mSharedPreferences;
    public static final String ANONYMOUS = "anonymous";
    private DatabaseReference mDatabase;
    private GoogleApiClient googleApiClient;
    private static final String TAG = "MonitoringActivity";



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set default username is anonymous.
        mUsername = ANONYMOUS;
        // Initialize Firebase Auth
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();

        if (mFirebaseUser == null) {
            // Not signed in, launch the Sign In activity
            startActivity(new Intent(this.getActivity(), SignInActivity.class));
            return;
        } else {
            mUsername = mFirebaseUser.getDisplayName();
            System.out.println(mFirebaseUser.getDisplayName());

            if (mFirebaseUser.getPhotoUrl() != null) {
                mPhotoUrl = mFirebaseUser.getPhotoUrl().toString();
            }
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_monitoring,
                container, false);


        createFragment();

        return view;
    }

    public void createFragment() {


    }

    public static MonitoringFragment newInstance() {
        MonitoringFragment fragmentDemo = new MonitoringFragment();
        return fragmentDemo;
    }

}
