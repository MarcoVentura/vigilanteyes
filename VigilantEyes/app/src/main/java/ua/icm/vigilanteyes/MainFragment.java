package ua.icm.vigilanteyes;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

/**
 * Created by Marco on 09/10/2017.
 */

public class MainFragment extends Fragment {

    private Integer []imagesID = {R.drawable.officer, R.drawable.camera_icon, R.drawable.heart,
        R.drawable.friend_icon, R.drawable.panic};
    private String [] nomes;
    private CustomListView customListView;
    private ListView listView;
    private String selectedActivity;

    private OnItemSelectedListener listener;

    public interface OnItemSelectedListener {
        public void onItemSelected(Integer pos);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof OnItemSelectedListener) {
            listener = (OnItemSelectedListener) context;
        } else {
            throw new ClassCastException(
                    context.toString()
                            + " must implement OnListItemSelectedListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate view
        View view = inflater.inflate(R.layout.fragment_activity_main, container,
                false);

        nomes = getResources().getStringArray(R.array.atividades);
        listView = (ListView) view.findViewById(R.id.atividadesID);
        customListView = new CustomListView(getActivity(), nomes, imagesID);

        listView.setAdapter(customListView);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View item, int position,
                                    long rowId) {
                // Retrieve item based on position
                selectedActivity = customListView.getItem(position);
                System.out.println(selectedActivity);
                System.out.println(position);

                listener.onItemSelected(position);
            }
        });
        return view;
    }

    /**
     * Turns on activate-on-click mode. When this mode is on, list items will be
     * given the 'activated' state when touched.
     */
    public void setActivateOnItemClick(boolean activateOnItemClick) {
        // When setting CHOICE_MODE_SINGLE, ListView will automatically
        // give items the 'activated' state when touched.
        listView.setChoiceMode(
                activateOnItemClick ? ListView.CHOICE_MODE_SINGLE
                        : ListView.CHOICE_MODE_NONE);
    }
}
