package ua.icm.vigilanteyes;

/**
 * Created by Marco on 11/11/2017.
 */

public class UserRoute {

    private Double DestinoLat;
    private Double DestinoLon;
    private Double OrigemLat;
    private Double OrigemLon;


    public UserRoute() {
    }

    public UserRoute(Double DestinoLat, Double DestinoLon, Double OrigemLat, Double OrigemLon) {
        this.DestinoLat = DestinoLat;
        this.DestinoLon = DestinoLon;
        this.OrigemLat = OrigemLat;
        this.OrigemLon = OrigemLon;
    }

    public Double getOrigemLat() {
        return OrigemLat;
    }

    public void setOrigemLat(Double origemLat) {
        OrigemLat = origemLat;
    }

    public Double getOrigemLon() {
        return OrigemLon;
    }

    public void setOrigemLon(Double origemLon) {
        OrigemLon = origemLon;
    }

    public Double getDestinoLat() {
        return DestinoLat;
    }

    public void setDestinoLat(Double destinoLat) {
        DestinoLat = destinoLat;
    }

    public Double getDestinoLon() {
        return DestinoLon;
    }

    public void setDestinoLon(Double destinoLon) {
        DestinoLon = destinoLon;
    }

    @Override
    public String toString() {
        return "UserRoute{" +
                "DestinoLat=" + DestinoLat +
                ", DestinoLon=" + DestinoLon +
                ", OrigemLat=" + OrigemLat +
                ", OrigemLon=" + OrigemLon +
                '}';
    }
}
