package ua.icm.vigilanteyes;

import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class CameraFragment extends Fragment {

    private static final int CAM_REQUEST = 1313;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_camera,
                container, false);

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent,CAM_REQUEST);

        return view;
    }


    public static CameraFragment newInstance()
    {
        CameraFragment fragmentDemo = new CameraFragment();
        return fragmentDemo;
    }
}
