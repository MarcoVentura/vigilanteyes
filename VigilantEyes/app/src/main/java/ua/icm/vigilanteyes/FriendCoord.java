package ua.icm.vigilanteyes;

/**
 * Created by Marco on 25/10/2017.
 */

public class FriendCoord {

    private String nome;
    private Double lat;
    private Double lon;


    public FriendCoord() {
    }

    public FriendCoord( String nome, Double lat, Double lon) {
        this.nome = nome;
        this.lat = lat;
        this.lon = lon;
    }

    public Double getLat() {
        return lat;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    @Override
    public String toString() {
        return "FriendCoord{" +
                ", nome='" + nome + '\'' +
                ", lat=" + lat +
                ", lon=" + lon +
                '}';
    }
}
