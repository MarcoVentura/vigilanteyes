package ua.icm.vigilanteyes;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.PlaceDetectionClient;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

/**
 * Created by Marco on 11/10/2017.
 */

public class MapViewFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener,
        LocationListener, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks{

    private SupportMapFragment fragment;
    private GoogleMap nMap;
    private static final int REQUEST_LOCATION = 2;

    private DatabaseReference mDatabase;
    private GoogleApiClient googleApiClient;
    private static final String TAG = "MapView";

    private double longitude;
    private double latitude;
    private double origem;
    private double destino;
    private LatLng currentLocation;

    // The entry points to the Places API.
    private GeoDataClient mGeoDataClient;
    private PlaceDetectionClient mPlaceDetectionClient;

    // The entry point to the Fused Location Provider.
    private FusedLocationProviderClient mFusedLocationProviderClient;

    private CameraPosition mCameraPosition;
    private static final int DEFAULT_ZOOM = 15;
    private Location mLastKnownLocation;
    private final LatLng mDefaultLocation = new LatLng(40.630653, -8.658035);

    // Firebase instance variables
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;

    public static final String ANONYMOUS = "anonymous";
    private String mUsername;

    ArrayList<FriendCoord> lista = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        int permissionCheck = ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION);

        if(permissionCheck != PackageManager.PERMISSION_GRANTED) {
            // ask permissions here using below code
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION );
        }

        FragmentManager fm = getChildFragmentManager();
        fragment = (SupportMapFragment) fm.findFragmentById(R.id.mapa);
        if (fragment == null) {
            fragment = SupportMapFragment.newInstance();
            fm.beginTransaction().replace(R.id.mapa, fragment).commit();
        }

        // Set default username is anonymous.
        mUsername = ANONYMOUS;
        // Initialize Firebase Auth
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();

        // Construct a GeoDataClient.
        mGeoDataClient = Places.getGeoDataClient(this.getActivity(), null);
        // Construct a PlaceDetectionClient.
        mPlaceDetectionClient = Places.getPlaceDetectionClient(this.getActivity(), null);
        // Construct a FusedLocationProviderClient.
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this.getActivity());

        mDatabase = FirebaseDatabase.getInstance().getReference();

        fragment.getMapAsync(this);

        mUsername = mFirebaseUser.getDisplayName();
    }

    @Override
    public void onStop() {
        super.onStop();

        final String userId = getUid();

        // save data on firebase db
        mDatabase.child("coordenadas").child(userId).child("nome").setValue(mUsername);
        mDatabase.child("coordenadas").child(userId).child("lat").setValue(0.0);
        mDatabase.child("coordenadas").child(userId).child("lon").setValue(0.0);
    }

    private void getDeviceLocation() {
        try {
                Task<Location> locationResult = mFusedLocationProviderClient.getLastLocation();
                locationResult.addOnCompleteListener(this.getActivity(), new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful()) {
                            // Set the map's camera position to the current location of the device.
                            mLastKnownLocation = task.getResult();
                           // System.out.println("latitute: " + mLastKnownLocation.getLatitude());
                            latitude = mLastKnownLocation.getLatitude();
                            longitude = mLastKnownLocation.getLongitude();

                            submitLocation();

                            nMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                                    new LatLng(mLastKnownLocation.getLatitude(),
                                            mLastKnownLocation.getLongitude()), DEFAULT_ZOOM));

                        } else {
                            Log.d(TAG, "Current location is null. Using defaults.");
                            Log.e(TAG, "Exception: %s", task.getException());
                            nMap.moveCamera(CameraUpdateFactory
                                    .newLatLngZoom(mDefaultLocation, DEFAULT_ZOOM));
                            nMap.getUiSettings().setMyLocationButtonEnabled(false);
                        }
                    }
                });
        } catch (SecurityException e)  {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    public String getUid() {
        return FirebaseAuth.getInstance().getCurrentUser().getUid();
    }

    public void submitLocation() {

        final String userId = getUid();

        // save data on firebase db
        mDatabase.child("coordenadas").child(userId).child("nome").setValue(mUsername);
        mDatabase.child("coordenadas").child(userId).child("lat").setValue(latitude);
        mDatabase.child("coordenadas").child(userId).child("lon").setValue(longitude);

        mDatabase.child("coordenadas").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot ds: dataSnapshot.getChildren())
                {
                    FriendCoord user = ds.getValue(FriendCoord.class);
                    System.out.println(user);
                    lista.add(user);
                }
                getRoute();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void getRoute()
    {
        mDatabase = FirebaseDatabase.getInstance().getReference();

        mDatabase.child("coordenadas").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                UserRoute route = null;

                for (DataSnapshot ds: dataSnapshot.getChildren())
                {

                    if (ds.getKey().matches(getUid()))
                    {
                        route = ds.getValue(UserRoute.class);
                        System.out.println(route);
                    }
                }
                System.out.println(route.toString());

                addMarkers(route);
            }
            @Override
            public void onCancelled(DatabaseError databaseError) { }
        });
    }

    public void addMarkers(UserRoute route) {

        // removes all markers
        nMap.clear();

        currentLocation = new LatLng(latitude, longitude);

        for (FriendCoord f : lista) {
            LatLng local = new LatLng(f.getLat(), f.getLon());
            nMap.addMarker(new MarkerOptions().position(local).title(f.getNome()).snippet("Segurança")).setTag(0);
        }

        if (route.getOrigemLat() == null || route.getDestinoLat() == null)
        {
            Toast.makeText(getActivity(), "Cria uma origem e destino do turno primeiro.", Toast.LENGTH_SHORT);
        }
        else
        {
            LatLng origem = new LatLng(route.getOrigemLat(), route.getOrigemLon());
            LatLng destino = new LatLng(route.getDestinoLat(), route.getOrigemLon());
            nMap.addMarker(new MarkerOptions().position(origem).title("Origem"));
            nMap.addMarker(new MarkerOptions().position(destino).title("Destino"));
        }

        // For zooming automatically to the location of the marker
        CameraPosition cameraPosition = new CameraPosition.Builder().target(currentLocation).zoom(15).build();
        nMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_map_view,
                container, false);
        return view;
    }

    public static MapViewFragment newInstance() {
        MapViewFragment fragmentDemo = new MapViewFragment();
        return fragmentDemo;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        nMap = googleMap;

        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED) {
            return;
        }
        nMap.setMyLocationEnabled(true);

        getDeviceLocation();

        nMap.setOnMarkerClickListener(this);

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    @Override
    public void onLocationChanged(Location location) {
        getDeviceLocation();
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

}
